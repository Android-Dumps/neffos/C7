#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/recovery:12436272:b52ebea5f4e9d437677e72e4150a8e4bc84400f3; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/boot:10595120:e0909012bb345371bd06ac5a19139c12d6e468b9 EMMC:/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/recovery b52ebea5f4e9d437677e72e4150a8e4bc84400f3 12436272 e0909012bb345371bd06ac5a19139c12d6e468b9:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
